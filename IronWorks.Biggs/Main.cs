﻿namespace IronWorks.Biggs
{
    using System;

    using CommandLine;

    /// <summary>
    ///   Yep. It's a main class alright.
    /// </summary>
    class MainClass
    {
        /// <summary>
        ///   This sure is a Main method.
        /// </summary>
        public static int Main(string[] args)
        {
            var log = new Logger(Console.Out, Console.Error);
            return CommandLine.Parser.Default
                .ParseArguments<ListOptions, UnpackOptions>(args)
                .MapResult(
                    (ListOptions opts) => ListVerb.Do(opts, log),
                    (UnpackOptions opts) => UnpackVerb.Do(opts, log),
                    errs => (int)ExitStatus.Failure);
        }
    }
}
