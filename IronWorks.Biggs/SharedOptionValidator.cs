namespace IronWorks.Biggs
{
    using System;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    ///   Validates options that are shared by several verbs.
    /// </summary>
    internal static class SharedOptionValidator
    {
        /// <summary>
        ///   Index file name validation.
        /// </summary>
        public static bool ValidateIndexFileName(string indexFileName, Logger log)
        {
            // So that we don't have to repeat ourselves...
            bool fail(string reason)
            {
                log.Error(
                    string.Format(
                        Strings.SharedOptionValidator_InvalidIndexOption,
                        indexFileName));
                log.Error(reason);
                return false;
            }

            FileInfo indexInfo = null;
            try
            {
                indexInfo = new FileInfo(indexFileName);
            }
            catch (Exception ex)
            {
                return fail(ex.Message);
            }

            if (!indexInfo.Exists)
            {
                return Directory.Exists(indexFileName)
                    ? fail(Strings.SharedOptionValidator_InvalidIndexOption_IsDirectory)
                    : fail(Strings.SharedOptionValidator_InvalidIndexOption_NoSuchFile);
            }

            if (!Regex.IsMatch(indexInfo.Extension, ".index2?"))
            {
                return fail(
                    Strings.SharedOptionValidator_InvalidIndexOption_BadExtension);
            }

            // While this seems like it would be a good safety measure, it
            // doesn't really work out. Weirdly, .NET considers things like
            // device nodes to be "normal" files. Moreover, if the user
            // doesn't have permission to access the file, it isn't
            // "Normal", yielding a misleading error description.
            //
            // var attributes = indexInfo.Attributes;
            // if ((attributes & FileAttributes.Normal)
            //     != FileAttributes.Normal)
            // {
            //     return fail("Not a normal file.");
            // }
            //
            // So ultimately we just have to poke the file to see if it's
            // readable and seekable. If this fails due to permissions or
            // whatever, the reson will bubble up as an exception message.
            FileStream fs = null;
            try
            {
                fs = indexInfo.Open(
                    FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            catch (Exception ex)
            {
                return fail(ex.Message);
            }
            if (!fs.CanRead)
            {
                return fail(
                    Strings.SharedOptionValidator_InvalidIndexOption_NotReadable);
            }
            if (!fs.CanSeek)
            {
                return fail(
                    Strings.SharedOptionValidator_InvalidIndexOption_NotReadable);
            }
            fs.Close();

            return true;
        }
    }
}
