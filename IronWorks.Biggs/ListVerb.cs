namespace IronWorks.Biggs
{
    using System.IO;

    using IronWorks.Parser;

    /// <summary>
    ///   "list" verb processing
    /// </summary>
    internal class ListVerb
    {
        /// <summary>
        ///   Performs the "list" verb.
        /// </summary>
        public static int Do(ListOptions options, Logger log)
        {
            if (!options.Validate(log))
            {
                return (int)ExitStatus.Failure;
            }

            var indexFileInfo = new FileInfo(options.IndexFileName);
            var indexFormat =
                (indexFileInfo.Extension == ".index")
                ? SqPackIndexFormat.Format1
                : SqPackIndexFormat.Format2;
            var pack = SqPackParser.Parse(
                options.IndexFileName, indexFormat);

            var mainHeaderStr =
                string.Format(
                    Strings.ListVerb_Output_MainHeader,
                    options.IndexFileName);
            log.Out.WriteLine(mainHeaderStr);
            log.Out.WriteLine(Separator(mainHeaderStr));

            var indent = Indent(options.IndentSize);
            foreach (var folder in pack.Folders)
            {
                log.Out.WriteLine();
                var folderHeader =
                    string.Format(
                        Strings.ListVerb_Output_FolderHeader,
                        folder.NameHash);
                log.Out.WriteLine(folderHeader);
                log.Out.WriteLine(Separator(folderHeader));
                log.Out.WriteLine();

                var fileTableHeader = Strings.ListVerb_Output_FileTableHeader;
                log.Out.WriteLine($"{indent}{fileTableHeader}");
                log.Out.WriteLine($"{indent}{Separator(fileTableHeader)}");

                foreach (var file in folder.Files)
                {
                    var fileRow =
                        string.Format(
                            Strings.ListVerb_Output_FileTableRow,
                            file.NameHash,
                            file.DataVolumeId,
                            file.DataOffset);
                    log.Out.WriteLine($"{indent}{fileRow}");
                }
            }

            return (int)ExitStatus.Success;
        }

        /// <summary>
        ///   Generates a identation of the specified length.
        /// </summary>
        private static string Indent(int size)
        {
            var buf = new char[size];
            for (var i = 0; i < buf.Length; ++i)
            {
                buf[i] = ' ';
            }
            return new string(buf);
        }

        /// <summary>
        ///   Generates a tab of the same length as <paramref
        ///   name="header"/>.
        /// </summary>
        private static string Separator(string header)
        {
            var buf = new char[header.Length];
            for (var i = 0; i < buf.Length; ++i)
            {
                buf[i] = '-';
            }
            return new string(buf);
        }
    }
}
