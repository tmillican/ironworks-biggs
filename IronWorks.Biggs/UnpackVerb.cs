namespace IronWorks.Biggs
{
    using System;
    using System.IO;

    using IronWorks.Monads;
    using IronWorks.Parser;
    using IronWorks.Structures;

    /// <summary>
    ///   "unpack" verb processing
    /// </summary>
    internal static class UnpackVerb
    {
        /// <summary>
        ///   Performs the "unpack" verb.
        /// </summary>
        /// <param name="options">
        ///   An <see cref="UnpackOptions"/> instance representing the parsed
        ///   command line options.
        /// </param>
        /// <param name="log">
        ///   An <see cref="Logger"/> instance to which output should be logged.
        /// </param>
        public static int Do(UnpackOptions options, Logger log)
        {
            if (!options.Validate(log))
            {
                return (int)ExitStatus.Failure;
            }

            var indexFileInfo = new FileInfo(options.IndexFileName);
            var indexFormat =
                (indexFileInfo.Extension == ".index")
                ? SqPackIndexFormat.Format1
                : SqPackIndexFormat.Format2;
            var pack = SqPackParser.Parse(
                options.IndexFileName, indexFormat);

            // Unpack all
            if (options.IsTargetAll)
            {
                return UnpackAll(pack, log);
            }

            // Unpack entire folder
            var targetParts = options.TargetHash.Split(':');
            if (targetParts.Length == 1)
            {
                return UnpackFolder(
                    pack,
                    long.Parse(
                        targetParts[0],
                        System.Globalization.NumberStyles.HexNumber),
                    log);
            }

            // Unpack single file
            return UnpackFile(
                pack,
                long.Parse(
                    targetParts[0],
                    System.Globalization.NumberStyles.HexNumber),
                long.Parse(
                    targetParts[1],
                    System.Globalization.NumberStyles.HexNumber),
                log);
        }

        /// <summary>
        ///   Unpacks all folders in the pack.
        /// </summary>
        private static int UnpackAll(ISqPack pack, Logger log)
        {
            foreach (var folder in pack.Folders)
            {
                var status = UnpackFolder(pack, folder.NameHash, log);
                if (status != (int)ExitStatus.Success)
                {
                    return status;
                }
            }
            return (int)ExitStatus.Success;
        }

        /// <summary>
        ///   Unpacks a single folder in the pack.
        /// </summary>
        private static int UnpackFolder(ISqPack pack, long folderNameHash, Logger log)
        {
            Func<None<ISqPackFolder>, int> noSuchFolder = nv => {
                log.Error(
                    string.Format(
                        Strings.UnpackVerb_Output_NoSuchSqPackFolder,
                        $"{folderNameHash:X8}"));
                return (int)ExitStatus.Failure;
            };

            Func<ISqPackFolder, int> unpack = folder => {
                foreach (var file in folder.Files)
                {
                    var status =
                        UnpackFile(pack, folderNameHash, file.NameHash, log);
                    if (status != (int)ExitStatus.Success)
                    {
                        return status;
                    }
                }
                return (int)ExitStatus.Success;
            };

            return pack.GetFolderByNameHash(folderNameHash)
                .Match(
                    some: unpack,
                    none: noSuchFolder);
        }

        /// <summary>
        ///   Unpacks a single file in the pack.
        /// </summary>
        private static int UnpackFile(
            ISqPack pack,
            long folderNameHash,
            long fileNameHash,
            Logger log)
        {
            Func<None<ISqPackFile>, int> noSuchFile = nv => {
                log.Error(
                    string.Format(
                        Strings.UnpackVerb_Output_NoSuchSqPackFile,
                        $"{folderNameHash:X8}:{fileNameHash:X8}"));
                return (int)ExitStatus.Failure;
            };

            Func<ISqPackFile, int> unpack = file => {
                var volumeName =
                $"{pack.Directory.FullName}/{pack.FileNameRoot}"
                + $".dat{file.DataVolumeId}";
                var dataStream = SqPackDataStreamParser.Parse(
                    volumeName,
                    file.DataOffset);
                var outputFileName =
                $"{folderNameHash:X8}-{fileNameHash:X8}"
                + $".{dataStream.FileExtension}";
                FileStream outStream;
                try
                {
                    outStream = new FileStream(
                        outputFileName,
                        FileMode.CreateNew,
                        FileAccess.Write,
                        FileShare.None);
                }
                catch (Exception ex)
                {
                    log.Error(
                        $"While creating {outputFileName}, encountered "
                        + $"{ex.GetType().FullName}: {ex.Message}");
                    return (int)ExitStatus.Failure;
                }
                try
                {
                    dataStream.Unpack(outStream);
                }
                catch (Exception ex)
                {
                    log.Error(
                        $"While writing to {outputFileName}, encountered "
                        + $"{ex.GetType().FullName}: {ex.Message}");
                    return (int)ExitStatus.Failure;
                }
                return (int)ExitStatus.Success;
            };

            return pack.GetFileByPathNameHash(
                folderNameHash,
                fileNameHash)
                .Match(
                    some: unpack,
                    none: noSuchFile);
        }
    }
}
