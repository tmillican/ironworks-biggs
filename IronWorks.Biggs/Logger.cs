namespace IronWorks.Biggs
{
    using System;
    using System.IO;

    /// <summary>
    ///   A simple logging facility
    /// </summary>
    public class Logger
    {
        /// <summary>
        ///   Gets the destination <see cref="TextWriter"/> for normal output.
        /// </summary>
        /// <value>
        ///   The destination <see cref="TextWriter"/> for normal output.
        /// </value>
        public TextWriter Out { get; }

        /// <summary>
        ///   Gets the destination <see cref="TextWriter"/> for error output.
        /// </summary>
        /// <value>
        ///   The destination <see cref="TextWriter"/> for error output.
        /// </value>
        public TextWriter Err { get; }

        /// <summary>
        ///   Initializes a new instance of the <see cref="Logger"/> class with
        ///   the specified destinations.
        /// </summary>
        /// <param name="outWriter">
        ///   The destination <see cref="TextWriter"/> for standard (normal,
        ///   informational) messages.
        /// </param>
        /// <param name="errorWriter">
        ///   The destination <see cref="TextWriter"/> for error (and warning)
        ///   messages.
        /// </param>
        public Logger(TextWriter outWriter, TextWriter errorWriter)
        {
            if (outWriter is null)
            {
                throw new ArgumentNullException(nameof(outWriter));
            }
            if (errorWriter is null)
            {
                throw new ArgumentNullException(nameof(errorWriter));
            }

            Out = outWriter;
            Err = errorWriter;
        }

        /// <summary>
        ///   Log an error message to the error destination.
        /// </summary>
        /// <param name="message">
        ///   The message to log.
        /// </param>
        public void Error(string message)
        {
            Err.WriteLine(string.Format(Strings.Logger_Error, message));
        }

        /// <summary>
        ///   Log an informational message to the standard destination.
        /// </summary>
        /// <param name="message">
        ///   The message to log.
        /// </param>
        public void Info(string message)
        {
            Out.WriteLine(string.Format(Strings.Logger_Info, message));
        }

        /// <summary>
        ///   Log a warning message to the error destination.
        /// </summary>
        /// <param name="message">
        ///   The message to log.
        /// </param>
        public void Warn(string message)
        {
            Err.WriteLine(string.Format(Strings.Logger_Warn, message));
        }
    }
}
