namespace IronWorks.Biggs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    using CommandLine;
    using CommandLine.Text;

    /// <summary>
    ///   Options for the "unpack" verb.
    /// </summary>
    [Verb("unpack", HelpText="Unpack contents from an SqPack index")]
    public class UnpackOptions
    {
        /// <summary>
        ///   Gets "unpack" usage examples.
        /// </summary>
        /// <summary>
        ///   An enumerable collection of <see cref="CommandLine.Text.Example"/>
        ///   instances for the "unpack" verb.
        /// </summary>
        [Usage(ApplicationAlias = "biggs")]
        public static IEnumerable<Example> Examples {
            get {
                yield return new Example(
                    "Unpack all files within '060000.win32.index'",
                    new UnpackOptions(
                        indexFileName: "060000.win32.index",
                        targetHash: null,
                        isTargetAll: true));

                yield return new Example(
                    "Unpack a specific file within '060000.win32.index'",
                    new UnpackOptions(
                        indexFileName: "060000.win32.index",
                        targetHash: "DEADBEEF:BADF00D",
                        isTargetAll: false));

                yield return new Example(
                    "Unpack a entire folder within '060000.win32.index'",
                    new UnpackOptions(
                        indexFileName: "060000.win32.index",
                        targetHash: "DEADBEEF",
                        isTargetAll: false));
            }
        }

        /// <summary>
        ///   Gets the index file name.
        /// </summary>
        /// <value>
        ///   A string containing the index file name.
        /// </value>
        [Option('i', "index", Required = true, HelpText = "The index file to process.")]
        public string IndexFileName { get; }

        /// <summary>
        ///   Gets a value indicating whether all files should be extracted.
        /// </summary>
        /// <value>
        ///   <code>true</code> if all files should be extracted,
        ///   <code>false</code> if only a specified path should be extracted..
        /// </value>
        /// <remarks>
        ///   <para>
        ///     This option is mutually exclusive with "--path-hash".
        ///   </para>
        /// </remarks>
        [Option('a', "target-all",
                SetName = "targetAll",
                Required = false,
                HelpText = "Specifies that all files should be extracted. "
                + "This options is mutually exclusive with --target-hash.")]
        public bool IsTargetAll { get; }

        /// <summary>
        ///   Gets the hash of the path to extract.
        /// </summary>
        /// <value>
        ///   A string the hash of the path to extract.
        /// </value>
        [Option('t', "target-hash",
                SetName = "targetHash",
                Required = false,
                HelpText = "Specifies a target path to extract by its name "
                + "hash. The hash must be in the format FOLDERHASH[:FILEHASH]. "
                + "Both FOLDERHASH and FILEHASH must be hex values without "
                + "a leading \"0x\". You may omit leading \"0\"s. If the "
                + "FILEHASH portion is omitted, the entire folder will be "
                + "unpacked. If --target-hash is not specified, --target-all"
                + "must be specified instead.")]
        public string TargetHash { get; }

        /// <summary>
        ///   Initializes a new instance of the <see cref="UnpackOptions"/> class
        ///   with the specified options.
        /// </summary>
        public UnpackOptions(
            string indexFileName,
            bool isTargetAll,
            string targetHash)
        {
            IndexFileName = indexFileName;
            IsTargetAll = isTargetAll;
            TargetHash = targetHash;
        }

        // Validation
        // =====================================================================

        /// <summary>
        ///   Performs additional validation above and beyond the capabilities
        ///   of <code>CommandLineParser</code>.
        /// </summary>
        public bool Validate(Logger log)
        {
            return true
                & SharedOptionValidator.ValidateIndexFileName(
                    IndexFileName, log)
                & ValidateTargetRequired(log)
                & ValidateTargetHash(log);
        }

        /// <summary>
        ///   Validates that '--target-hash' is a valid hash pattern.
        /// </summary>
        private bool ValidateTargetHash(Logger log)
        {
            // This option isn't required since --target-all may be specified
            // instead. By this point, we've already ensured that one or the
            // other is specified.
            if (TargetHash is null)
            {
                return true;
            }

            if (!Regex.IsMatch(
                    TargetHash,
                    "^[a-fA-F0-9]{1,8}(:[a-fA-F0-9]{1,8})?$"))
            {
                log.Error(
                    string.Format(
                        Strings.UnpackVerb_InvalidTargetHash,
                        TargetHash));
                return false;
            }
            return true;
        }

        /// <summary>
        ///   Verify that either '--target-hash' or '--target-all' was specified.
        /// </summary>
        private bool ValidateTargetRequired(Logger log)
        {
            if ((TargetHash is null) && !IsTargetAll)
            {
                log.Error(Strings.UnpackVerb_TargetRequired);
                return false;
            }
            return true;
        }
    }
}
