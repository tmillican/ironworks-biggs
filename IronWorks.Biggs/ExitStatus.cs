namespace IronWorks.Biggs
{
    /// <summary>
    ///   Enum defining valid exit status codes.
    /// </summary>
    public enum ExitStatus
    {
        /// <summary>
        ///   Exit status code for successful execution.
        /// </summary>
        Success = 0,

        /// <summary>
        ///   Exit status code for general failure.
        /// </summary>
        Failure = 1
    }
}
