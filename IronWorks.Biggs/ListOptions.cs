namespace IronWorks.Biggs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    using CommandLine;
    using CommandLine.Text;

    /// <summary>
    ///   Options for the "list" verb.
    /// </summary>
    [Verb("list", HelpText="List the contents of an SqPack index")]
    public class ListOptions
    {
        /// <summary>
        ///   Gets "list" usage examples.
        /// </summary>
        /// <summary>
        ///   An enumerable collection of <see cref="CommandLine.Text.Example"/>
        ///   instances for the "list" verb.
        /// </summary>
        [Usage(ApplicationAlias = "biggs")]
        public static IEnumerable<Example> Examples {
            get {
                yield return new Example(
                    "List all contents of '060000.win32.index'",
                    new ListOptions("060000.win32.index", 0));

                yield return new Example(
                    "List all contents of '060000.win32.index' with indentation",
                    new ListOptions("060000.win32.index", 4));
            }
        }

        /// <summary>
        ///   Gets the index file name.
        /// </summary>
        /// <value>
        ///   A string containing the index file name.
        /// </value>
        [Option('i', "index", Required = true, HelpText = "The index file to process.")]
        public string IndexFileName { get; }

        /// <summary>
        ///   Gets a value indicating whether output should be indented.
        /// </summary>
        [Option('n', "indent-size",
                Default = 0,
                HelpText = "Specifies the amount (in spaces) by which file "
                + "records are indented in the output.")]
        public int IndentSize { get; }

        /// <summary>
        ///   Initializes a new instance of the <see cref="ListOptions"/> class
        ///   with the specified options.
        /// </summary>
        public ListOptions(string indexFileName, int indentSize)
        {
            IndexFileName = indexFileName;
            IndentSize = indentSize;
        }

        /// <summary>
        ///   Additional option validation above and beyond the capabilities of
        ///   <code>CommandLineParser</code>.
        /// </summary>
        public bool Validate(Logger log)
        {
            return true
                & SharedOptionValidator.ValidateIndexFileName(
                    IndexFileName, log)
                & ValidateIndentSize(log);
        }

        /// <summary>
        ///   Indent size validation.
        /// </summary>
        private bool ValidateIndentSize(Logger log)
        {
            if (IndentSize < 0)
            {
                log.Error(string.Format(
                              Strings.ListVerb_InvalidIndentSizeOption,
                              IndentSize));
                log.Error(Strings.ListVerb_InvalidIndentSizeOption_NonNegativeRequired);
                return false;
            }
            return true;
        }
    }
}
