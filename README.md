### IronWorks.Biggs

## Project Description

__IronWorks.Biggs__ is a traditional \*NIX style command-line tool for
exploring and extracting data from SqPack format archives.

## Features

Currently, Biggs supports listing and unpacking the contents of SqPack archives
(.index and .dat files).

## License

__IronWorks.Biggs__ is released under the MIT License ([SPDX MIT][3]). A
[markdown version][4] of the license is provided in the repository.

[3]:https://spdx.org/licenses/MIT.html
[4]:LICENSE.md
